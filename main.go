package main

import (
	"os"
	"net/http"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/remotecommand"
	"fmt"
	"bytes"
	"strconv"
	"time"
	"flag"
	"path/filepath"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/kubernetes"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"strings"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
)

var (
	message = "total_pvc_usage_kb 1\n"
)

func main() {

	recordMetrics()

	http.HandleFunc("/metrics", func (w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(message))
	})

	http.ListenAndServe(":4567", nil)

}


func recordMetrics() {
	go func() {

		var kubeconfig *string
		if home := homeDir(); home != "" {
			kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
		} else {
			kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
		}
		flag.Parse()

		// use the current context in kubeconfig
		config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
		if err != nil {
			panic(err.Error())
		}

		// create the clientset
		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			panic(err.Error())
		}

		for {
			pods, err := clientset.CoreV1().Pods("").List(metav1.ListOptions{})
			if err != nil {
				panic(err.Error())
			}
			fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))

			var m = make(map[string][4]int64)
			var total int64 = 0
			var totalClaim int64 = 0
			var msg string = ""
			for _, pod := range pods.Items {
				fmt.Printf("Pod %s \n", pod.Name)
				for _, volume := range pod.Spec.Volumes {
					if volume.PersistentVolumeClaim != nil {
						var claimName = volume.PersistentVolumeClaim.ClaimName
						if _, ok := m[claimName]; !ok {
							fmt.Printf("pvc[%s] in pod[%s] \n", claimName, pod.Name)

							for _, container := range pod.Spec.Containers {
								for _, volumeMount := range container.VolumeMounts {
									if volumeMount.Name == volume.Name {

										req := clientset.CoreV1().RESTClient().Post().
											Resource("pods").Name(pod.Name).Namespace(pod.Namespace).
											SubResource("exec")

										req.VersionedParams(&v1.PodExecOptions{
											Container: container.Name,
											Command:   []string{"df", volumeMount.MountPath},
											Stdout:    true,
											Stderr:    true,
										}, scheme.ParameterCodec)

										exec, err := remotecommand.NewSPDYExecutor(config, "POST", req.URL())
										if err != nil {
											fmt.Printf("failed to init executor: %v", err)
										}

										var (
											execOut bytes.Buffer
											execErr bytes.Buffer
										)
										err = exec.Stream(remotecommand.StreamOptions{
											Stdout: &execOut,
											Stderr: &execErr,
											Tty:    false,
										})

										if err != nil {
											fmt.Printf("could not execute: %v", err)
										} else {
											var df = execOut.String()
											fmt.Println(df)
											lines := strings.Split(strings.TrimSuffix(df, "\n"), "\n")
											tokens := strings.Fields(lines[1])
											//fmt.Println(tokens)
											if len(tokens) > 4 {
												x1, _ := strconv.ParseInt(tokens[1], 10, 64)
												x2, _ := strconv.ParseInt(tokens[2], 10, 64)
												x3, _ := strconv.ParseInt(tokens[3], 10, 64)
												x4, _ := strconv.ParseInt(strings.Replace(tokens[4],"%", "", 1), 10, 64)
												m[claimName] = [...]int64{x1, x2, x3, x4}

												msg = msg + "pvc_all_kb{pvc=\"" + claimName + "\"} " + strconv.FormatInt(x1, 10) + "\n"
												msg = msg + "pvc_used_kb{pvc=\"" + claimName + "\"} " + strconv.FormatInt(x2, 10) + "\n"
												msg = msg + "pvc_available_kb{pvc=\"" + claimName + "\"} " + strconv.FormatInt(x3, 10) + "\n"
												msg = msg + "pvc_used_percentage{pvc=\"" + claimName + "\"} " + strconv.FormatInt(x4, 10) + "\n"

												total = total + x2
												totalClaim = totalClaim + x1

												fmt.Println(m[claimName])
											}

											break
										}

										if execErr.Len() > 0 {
											fmt.Printf("stderr: %v", execErr.String())
										}

									}
								}
							}
						}
					}
				}
			}

			msg = msg + "total_pvc_usage_kb " + strconv.FormatInt(total, 10) + "\n"
			msg = msg + "total_pvc_claim_kb " + strconv.FormatInt(totalClaim, 10) + "\n"
			message = msg
			fmt.Println(message)

			time.Sleep(60 * time.Second)
		}


	}()
}

func homeDir() string {
	if h := os.Getenv("HOME"); h != "" {
		return h
	}
	return os.Getenv("USERPROFILE") // windows
}